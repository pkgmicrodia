# Copyright 2008-2009 Vasily Khoruzhick
# Distributed under the terms of the GNU General Public License v2

inherit git eutils linux-mod

EGIT_REPO_URI="http://repo.or.cz/r/microdia.git"
# EGIT_BRANCH="origin/development"
# EGIT_TREE="origin/development"


DESCRIPTION="microdia - driver for sn9c20x-based webcams"
HOMEPAGE="http://groups.google.com/group/microdia"
SRC_URI=""

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"

MODULE_NAMES="sn9c20x(usb/video:)"
CONFIG_CHECK="VIDEO_DEV"
BUILD_TARGETS="all"

src_unpack() {
	git_src_unpack
	cd ${S}

	# Add any specific patches here
	# epatch ${FILESDIR}/0001-Cool-patch.patch
}
