%define tarball_name microdia

Name:           sn9c20x-common
Version:        v2009.01
Release:        1%{?dist}
Summary:        Common parts belonging to the sn9c20x Webcam Kernel Module

Group:          System Environment/Kernel
License:        GPLv2+
URL:            http://groups.google.com/microdia
Source0:        %{tarball_name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       sn9c20x-kmod >= %{version}
Provides:       sn9c20x-kmod-common = %{version}-%{release}
BuildArch:      noarch

%description
This RPM contains the common parts belonging to the sn9c20x Webcam Kernel Module,
which provides support for different sn9c20x-based webcams not included in the
default kernel

%prep
%setup -c -q

%build
iconv -f ISO_8859-1 -t UTF-8 microdia/README --output microdia/README.utf8
cp -af microdia/README.utf8 microdia/README

%install
rm -rf $RPM_BUILD_ROOT
mkdir $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc microdia/README

%changelog
* Tue Jan 11 2009 GWater <grewater@googlemail.com>
- New upstream release: v2009.01
* Tue Sep 16 2008 GWater <grewater@googlemail.com>
- Initial release
