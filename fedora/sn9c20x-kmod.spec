# (un)define the next line to either build for the newest or all current kernels
#%define buildforkernels newest
%define buildforkernels current
#%define buildforkernels akmod

%define tarball_name microdia

Name:           sn9c20x-kmod
Version:        v2009.01
Release:        1%{?dist}
Summary:        SN9C20x USB 2.0 Webcam Driver
Group:          System Environment/Kernel
License:        GPLv2+
URL:            http://groups.google.com/microdia
Source0:        %{tarball_name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# needed for plague to make sure it builds for i586 and i686
ExclusiveArch:  i586 i686 x86_64 ppc
# get the needed BuildRequires (in parts depending on what we build for)
BuildRequires:  %{_bindir}/kmodtool
%{!?kernels:BuildRequires: buildsys-build-rpmfusion-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }
# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }

%description
This RPM contains the %{summary} binary Linux kernel module build for %{kernel}. It
provides support for several different sn9c20x-based webcams not included in the default
kernel.


%prep
# error out if there was something wrong with kmodtool
%{?kmodtool_check}
# print kmodtool output for debugging purposes:
kmodtool  --target %{_target_cpu} --repo rpmfusion --kmodname %{name} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null

# go
%setup -q -c -T -a 0
for kernel_version  in %{?kernel_versions}; do
    cp -a %{tarball_name} _kmod_build_${kernel_version%%___*}
done


%build
for kernel_version in %{?kernel_versions}; do
    make V=1 -C "${kernel_version##*___}" SUBDIRS=${PWD}/_kmod_build_${kernel_version%%___*}
done


%install
rm -rf $RPM_BUILD_ROOT
for kernel_version in %{?kernel_versions}; do
    install -D -m 755 _kmod_build_${kernel_version%%___*}/sn9c20x.ko $RPM_BUILD_ROOT%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/sn9c20x.ko
done

%{?akmod_install}


%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Sun Jan 11 2009 GWater <grewater@googlemail.com>
- New upstream release: v2009.01
* Tue Sep 16 2008 GWater <grewater@googlemail.com>
- Initial release
